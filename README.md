# JSON [Android-Kotlin]

## What?
A module that allows to do useful things with JSON.
For example posting a json object to a remote http endpoint.

## Who?
Ken Ekeoha (ekeohak@gmail.com)

## How?
Build the module with the task "AssembleRelease" and link in your app module.

## May I?
MIT License

## But...?
For any questions, suggestions, remarks, please email me: ekeohak@gmail.com

  
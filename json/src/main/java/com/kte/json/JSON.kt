package com.kte.json

import okhttp3.*
import com.google.gson.Gson
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.IOException
import java.util.*

class JSON {

    companion object {

        private var debug_mode: Boolean = false

        private fun D(message: String) {
            if (debug_mode) {
                var date = Date().toString()
                print("[${date}] ${message}")
            }
        }

        public fun set_debug_mode(debug_mode: Boolean) {
            JSON.Companion.debug_mode = debug_mode
        }

        public fun post(message: String, url: String, callback: (String) -> Unit) {
            D("JSON: sending $message to $url...")
            var client = OkHttpClient.Builder().build()
            D("JSON: Http sender created.")
            var media_type = "application/json; charset=UTF-8".toMediaTypeOrNull();
            D("JSON: Media type defined. it is ${media_type.toString()}")
            var rbody = message.toRequestBody(media_type)
            D("JSON: Request Body created from message.")
            var request = Request.Builder().url(url).post(rbody).build()
            D("JSON: Request Created.")
            client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    D("JSON: Request Failed. error: ${e.message}")
                }

                override fun onResponse(call: Call, response: Response) {
                    D("JSON: Response received. Analyzing response...")
                    if (response.body != null) {
                        val response_body = response.body!!.string()
                        D("JSON: The response is $response_body.")
                        callback.invoke(response_body)
                    }
                }
            })
            D("JSON: Request Sent.")
        }

        public fun post(message: Any, url: String) {
            val jsonData = Gson().toJson(message)
            post(jsonData, url);
        }
    }
}